# Django PKCS7 Demo

```
git clone https://gitlab.com/rhab/dj-pkcs7/
cd dj-pkcs7
pip install . 
cd demo
python manage.py migrate 
python manage.py collectstatic
python manage.py runserver
```

## Next steps

disable DEBUG and review STATIC_ROOT in settings.py
