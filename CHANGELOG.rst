Changes
=======

0.X.0 (unreleased)
------------------
- ...


0.11.2 (21-03-2021)
-------------------
- fix: list items in result view

0.11.0 (16-03-2021)
-------------------
- add: template tag for hex printing serial

0.10.0 (16-03-2021)
-------------------
- change: README in markdown

0.7.0 (15-03-2021)
------------------
- add: support for .msg (using extract-msg)

0.1.0 (14-03-2021)
------------------
- initial version
