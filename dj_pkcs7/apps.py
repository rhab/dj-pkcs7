from django.apps import AppConfig


class DjPkcs7Config(AppConfig):
    name = 'dj_pkcs7'
